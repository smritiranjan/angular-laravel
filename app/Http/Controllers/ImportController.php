<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\DemoImport;
use JWTAuthException;
class ImportController extends Controller
{   
    public function __construct(){

    }

    public function uploadData(Request $request){
        if($request->hasFile('file')){
            $path = $request->file->getRealPath();
            $data = \Excel::load($path)->get();
            $data = $data->toArray();
            if(isset($data[0]) && count($data[0])){
                foreach ($data[0] as $key => $value) {
                    $arr[] = ['name' => $value['name'], 'email' => $value['email'], 'phone_no' => $value['phone_no'], 'address' => $value['address'], 'salary' => $value['salary']];
                }
                if(!empty($arr)){
                    $DemoImport = DemoImport::insert($arr);
                    return response()->json(['message' => 'Imported Successfully']);
                } else{
                    return response()->json(['message' => 'Not Imported']);
                }
            } else{
                return response()->json(['message' => 'No data Found']);
            }
        } else{
            return response()->json(['message' => 'Not file uploaded']);
        }
    }

    public function getImports(Request $request){
        $user = DemoImport::all();
        return response()->json(['result' => $user]);
    }

}