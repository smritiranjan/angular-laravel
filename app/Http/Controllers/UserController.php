<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use JWTAuthException;
class UserController extends Controller
{   
    private $user;
    public function __construct(User $user){
        $this->user = $user;
    }
   
    public function register(Request $request){
        $user = $this->user->create([
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'password' => bcrypt($request->get('password'))
        ]);
        return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
    }
    
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['message' => 'Invalid Email or password']);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token']);
        }
        return response()->json(compact('token'));
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    public function getUsers(Request $request){
        $getAuthUser = $this->getAuthUser($request);
        $user = User::where('id','<>', $getAuthUser->getData()->result->id)->get();
        return response()->json(['result' => $user]);
    }

    public function deleteUser(Request $request){
        $user = User::where('id', $request->input('id'))->delete();
        return response()->json(['result' => $user]);
    }

    public function userDetails(Request $request){
        $user = User::where('id', $request->input('id'))->first();
        return response()->json(['result' => $user]);
    }

    public function userUpdate(Request $request){
        $validator = \Validator::make($request->input('data'), [
            'name' => 'required',
            'email' => 'required',
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $req = $request->input('data');
        $user = User::where('id', $req['id'])->first();
        $user->name = $req['name'];
        $user->email = $req['email'];
        $user->updated_at = date('Y-m-d H:i:s');
        if($req['password']) {
            $user->password = bcrypt($req['password']);
        }

        if($user->update()) {
            return response()->json(['message'=>'Record is successfully Updated']);
        } else{
            return response()->json(['message'=>'Record is not updated']);
        }
        
    }
    public function userAdd(Request $request){
        $validator = \Validator::make($request->input('data'), [
            'name' => 'required',
            'password' => 'required',
            'email' => 'required|unique:users',
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->first()]);
        }
        $req = $request->input('data');
        $user = $this->user->create([
          'name' => $req['name'],
          'email' => $req['email'],
          'password' => bcrypt($req['password']),
          'created_at' => date('Y-m-d H:i:s')
        ]);        
        return response()->json(['message'=>'Record is successfully added']);
        
        
    }
}