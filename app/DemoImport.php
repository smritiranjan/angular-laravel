<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoImport extends Model
{
    protected $table = 'demoimports';

    public $fillable = ['name','email','phone_no','address', 'salary'];
}
