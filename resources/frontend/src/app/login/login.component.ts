import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../service/data.service';
import { AuthguardService } from '../service/authguard.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [DataService]
})

export class LoginComponent implements OnInit {
  //@ViewChild('username') el: ElementRef;
  statuslogin: any;
  focusin: boolean = true;
  rForm: FormGroup;
  post: any;
  showSpinner: boolean = false;
  usernameAlert: string = "Please fill username";
  passwordAlert: string = "Please fill password";
  loginAlert: string;
  loginError: boolean = false;
  returnUrl: string;
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private authenticationservice: DataService,
    public router: Router,
    public authGuard: AuthguardService
  ) {
    this.rForm = fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required],
    });
  }
  ngOnInit() {
    //this.authenticationservice.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/index';
    if (localStorage.getItem('currentUser')) {
      this.router.navigate([this.returnUrl]);
    }
    
  }

  addPost(post) {
    this.showSpinner = true;
    this.authenticationservice.login(post).subscribe(
      res => {
        this.showSpinner = false;
        if (res.token) {
          this.router.navigate([this.returnUrl]);
        } else {
           this.loginError = true
          this.loginAlert = res.message;
        }
      },
      err => {
        this.showSpinner = false;
        return err;
      }
    );

  }

}