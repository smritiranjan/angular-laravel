import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../service/data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthguardService } from '../service/authguard.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  public user = [];
  rForm: FormGroup;
  showSpinner: boolean = false;
  loginAlert: string;
  loginError: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    public authGuard: AuthguardService
  ) {

    this.route.params.subscribe(params => {
      console.log(params);
      this.rForm = fb.group({
        'email': ['', Validators.required],
        'password': [''],
        'name': ['', Validators.required],
        'id': ['', Validators.required],
      });
      if (params['id']) {
        this.dataService.getUserDetails(params['id']).subscribe((data: any[]) => {
          this.user = data;
          this.rForm = fb.group({
            'email': [this.user['result'].email, Validators.required],
            'password': [''],
            'id': [this.user['result'].id],
            'name': [this.user['result'].name, Validators.required],
          });
        })
      } else {
        this.router.navigate(['index']);
      }
    });
  }

  ngOnInit() {

  }

  addPost(put) {
    this.showSpinner = true;
    this.dataService.updateUser(put).subscribe(
      res => {
        this.showSpinner = false;
        this.loginError = true
        this.loginAlert = res.message;
      },
      err => {
        this.showSpinner = false;
        return err;
      }
    );

  }

}
