import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../service/data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthguardService } from '../service/authguard.service';

@Component({
  selector: 'app-table-import',
  templateUrl: './table-import.component.html',
  styleUrls: ['./table-import.component.scss']
})
export class TableImportComponent implements OnInit {
  form: FormGroup;
  error: string;
  userId: number = 1;
  showSpinner: boolean = false;
  loginAlert: string;
  loginError: boolean = false;
  uploadResponse = { status: '', message: '', filePath: '' };
  public dataSource: any;
  public displayedColumns: string[] = ['id', 'name', 'email', 'phone_no', 'address', 'salary'];
  @ViewChild('fileUpload', {static: false})
  fileUpload: ElementRef
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    public authGuard: AuthguardService
  ) {
    this.form = this.fb.group({
      avatar: ['']
    });

    this.dataService.getImports().subscribe((data) => {
      this.dataSource = data;
    })
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.fileUpload)
      this.fileUpload.nativeElement.click()
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.showSpinner = true;
      const file = event.target.files[0];
      this.form.get('avatar').setValue(file);
      const formData = new FormData();
      formData.append('file', this.form.get('avatar').value);
      this.dataService.upload(formData).subscribe(
        res => {
          this.showSpinner = false;
          this.loginError = true
          this.loginAlert = 'Imported Successfully';
        },
        err => {
          this.showSpinner = false;
          return err;
        }
      );
    }
  }

  onSubmit1() {

  }
}
