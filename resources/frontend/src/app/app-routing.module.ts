import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthguardService } from './service/authguard.service';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { TableImportComponent } from './table-import/table-import.component';

const routes: Routes = [

  { path: '', component: LoginComponent },
  {
    path: 'index', component: HomeComponent, canActivate: [AuthguardService],
    children: [
      { path: '', redirectTo: '', pathMatch: 'full' }
    ]
  },
  {
    path: 'user/edit/:id', component: EditUserComponent, canActivate: [AuthguardService]
  },
  {
    path: 'user/add', component: AddUserComponent, canActivate: [AuthguardService],
    children: [
      { path: '', redirectTo: '', pathMatch: 'full' }
    ]
  },
  {
    path: 'import', component: TableImportComponent, canActivate: [AuthguardService],
    children: [
      { path: '', redirectTo: '', pathMatch: 'full' }
    ]
  },


  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
