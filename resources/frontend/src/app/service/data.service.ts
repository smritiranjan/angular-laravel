import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class DataService {
    private token = "";
    private url: string = 'http://127.0.0.1:8000/api/';
    constructor(private http: HttpClient) {
        let localData = localStorage.getItem('currentUser');
        if (localData !== null) {
            this.token = JSON.parse(localData).token;
        }
    }

    login(data) {
        let username = data.username;
        let password = data.password;
        return this.http.post<any>(this.url + 'auth/login', { email: data.username, password: data.password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            }));
    }

    deleteUser(id) {
        return this.http.delete<any>(this.url + 'user/delete?token=' + this.token + '&id=' + id, {})
            .pipe(map(user => {
                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    getUsers() {
        return this.http.get(this.url + 'user?token=' + this.token);
    }

    getUserDetails(id) {
        return this.http.get(this.url + 'user/details?token=' + this.token + '&id=' + id);
    }

    updateUser(data) {
        return this.http.put<any>(this.url + 'user/update?token=' + this.token, {data})
            .pipe(map(user => {
                return user;
            }));
    }
    addUser(data) {
        return this.http.post<any>(this.url + 'user/add?token=' + this.token, {data})
            .pipe(map(user => {
                return user;
            }));
    }

    public upload(data) {
        return this.http.post<any>(this.url+'upload?token=' + this.token, data, {reportProgress: true,observe: 'events'})
        .pipe(map(data => {
            return data;
        }));
      }

    public getImports() {
        return this.http.get(this.url + 'imports?token=' + this.token);
      }
}