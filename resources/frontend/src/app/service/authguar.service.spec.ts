import { TestBed } from '@angular/core/testing';

import { AuthguarService } from './authguar.service';

describe('AuthguarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthguarService = TestBed.get(AuthguarService);
    expect(service).toBeTruthy();
  });
});
