import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public users = [];
  public dataSource: any;
  public displayedColumns: string[] = ['id', 'name', 'email', 'action'];
  constructor(
    private dataService: DataService,
    public dialog: MatDialog,
    public router: Router
  ) {
    this.dataService.getUsers().subscribe((data) => {
      this.dataSource = data;
    })
  }

  ngOnInit() {

  }


  logout() {
    this.dataService.logout();
    this.router.navigate(['']);
  }

  openDialog1(action, passdata) {
    if (action === "Delete") {
      if (confirm("Are you sure to delete ")) {
        this.dataService.deleteUser(passdata.id).subscribe((data) => {
          if (data['result'] === 1) {
            this.dataSource['result'] = this.dataSource['result'].filter((value, key) => {
              return value.id != passdata.id;
            });
          }
        })
      }
    } else if(action === "Update") {
      this.router.navigate(['user/edit']);
    } else {
      this.router.navigate(['user/add']);
    }
  }
  openDialog(action, obj) {
    if (action === "Delete") {
      obj.action = action;
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '250px',
        data: obj
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result.event == 'Delete') {
          this.deleteRowData(result.data);
        }
      });
    } else if(action === "Update") {
      this.router.navigate(['user/edit/',obj.id]);
    } else{
      this.router.navigate(['user/add']);
    }
  }

  deleteRowData(passdata) {
    this.dataService.deleteUser(passdata.id).subscribe((data: any[]) => {
      if (data['result'] === 1) {
        this.dataSource['result'] = this.dataSource['result'].filter((value, key) => {
          return value.id != passdata.id;
        });
      }
    })
  }

}
