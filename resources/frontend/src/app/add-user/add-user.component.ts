import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../service/data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthguardService } from '../service/authguard.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  public user = [];
  rForm: FormGroup;
  showSpinner: boolean = false;
  loginAlert: string;
  loginError: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    public authGuard: AuthguardService
  ) {

    this.rForm = fb.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required],
      'name': ['', Validators.required],
    });
  }

  ngOnInit() {

  }

  addPost(post) {
    this.showSpinner = true;
    this.dataService.addUser(post).subscribe(
      res => {
        this.showSpinner = false;
        this.loginError = true
        this.loginAlert = res.message;

      },
      err => {
        this.showSpinner = false;
        return err;
      }
    );

  }

}

